ARG JULIA_VERSION
FROM julia:${JULIA_VERSION}-bullseye

# Install required system packages.

RUN apt-get update && apt-get install -y --no-install-recommends \
  curl \
  unzip \
  # for PackageCompiler
  g++ \
  # for use of CLI Git with Pkg
  git \
  # to install packages over SSH (if you have any)
  openssh-client

RUN rm -rf /var/lib/apt/lists/*

# Install AWS utilities.

## Install the AWS CLI.
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
  unzip -qq awscliv2.zip && \
  rm -f awscliv2.zip && \
  ./aws/install

## Install the AWS Lambda Runtime Emulator interface.
## The version has been hardcoded here.
ENV AWS_LAMBDA_RIE /usr/local/bin/aws-lambda-rie
ARG ARCHITECTURE="x86_64"
RUN curl -L https://github.com/aws/aws-lambda-runtime-interface-emulator/releases/download/v1.10/aws-lambda-rie-$ARCHITECTURE --output $AWS_LAMBDA_RIE \
  && chmod +x $AWS_LAMBDA_RIE

# Install the application.

WORKDIR /var/task
COPY . .

## Explicit the depot used for managing Julia-related state.
ENV JULIA_DEPOT_PATH /var/task/.julia

## Build the system image.
ENV JULIA_PKG_USE_CLI_GIT true
RUN julia --color=yes --project=sysimage -e "using Pkg; Pkg.instantiate()"
RUN JULIA_DEBUG=PackageCompiler julia --color=yes --threads auto --project=sysimage sysimage/create_sysimage.jl \
  --precompile_execution_file test/runtests.jl \
  --cpu_target "generic;haswell,-rdrnd,-xsaveopt,clone_all;icelake-server,-rdrnd,base(1)" \
  --output JuliaProdSysimage.so
## Print the size of the image once built.
RUN du -sh /var/task/JuliaProdSysimage.so

## Make sure everything works.
## Do not remove, this will also download any lazy artifacts at `__init__` time if needed.
RUN julia --color=yes --project --sysimage JuliaProdSysimage.so -e "@time using AWSExample"

# Prepare the Docker image for execution.

WORKDIR /var/runtime
## Add bootstrap and entrypoint scripts.
COPY scripts/bootstrap .
COPY scripts/lambda-entrypoint.sh /
## /var/task is a read-only path during runtime.
## We need to allow another (writable) path for the depot.
ENV JULIA_DEPOT_PATH /tmp/.julia:/var/task/.julia

## Create an empty extensions directory.
WORKDIR /opt/extensions

## Set the entrypoint for this Docker image.
ENTRYPOINT ["/lambda-entrypoint.sh"]
## Provide default arguments/flags for the entrypoint command.
CMD [""]
