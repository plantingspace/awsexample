using AWSExample
using HTTP
using JSON3
using Sockets: getipaddr
using Test

endpoint(host, port, path = ""; protocol = "http") =
  string(protocol, "://", host, ':', port, path)

include("runtime.jl")
include("gateway.jl")

ENV["JULIA_DEBUG"] = "AWSExample,runtime,gateway"

@testset "AWSExample.jl" begin
  runtime_host = "127.0.0.1"
  runtime_port = 7574
  gateway_host = "127.0.0.1"
  gateway_port = 8081
  resp = LambdaResponder(; api_address = "$runtime_host:$runtime_port")
  aws = AWSRuntimeEmulator(runtime_host, runtime_port)
  gateway = APIGateway(aws; host = gateway_host, port = gateway_port)
  start(gateway)
  start(aws)
  start(resp)
  try
    @test !istaskfailed(resp.task)
    answer = HTTP.get(endpoint(gateway, "/unknown"), retries = 0, status_exception = false)
    @test HTTP.iserror(answer) && contains(String(copy(answer.body)), "Unexpected function 'unknown'")
    answer = HTTP.get(endpoint(gateway, "/test"), retries = 0)
    version = parse(VersionNumber, JSON3.read(answer.body)[:package_version])
    @test isa(version, VersionNumber)
    answer = HTTP.get(endpoint(gateway, "/peakflops"); body = "{\"worksize\": 2000}", retries = 0)
    output = JSON3.read(answer.body)
    @test isa(output, Float64)
    @test output ≥ 2.0
    answer = HTTP.get(endpoint(gateway, "/eval"); body = "{\"code\": \"binomial(6, 2)\"}", retries = 0)
    output = JSON3.read(answer.body)
    @test output === 15
  finally
    stop(gateway)
    stop(aws)
    stop(resp)
  end
end;
