import AWSExample: start, stop
using AWSExample: InvalidJSONException, BASE_HEADERS
using Base64: base64encode

"""
Emulated AWS Lambda runtime environment.

It is designed to interact with `LambdaResponder`, such that the latter can be
fully tested with a minimal component that emulates the AWS Runtime interface.

Public functions are:
- `start`
- `stop`
- `call_lambda`

This emulator works by buffering queries to be submitted to the `LambdaResponder`, and waiting until they are handled to then return an appropriate HTTP response to the caller.

Communication with the `LambdaResponder` is performed via the expected endpoints.
"""
mutable struct AWSRuntimeEmulator
  const host::String
  const port::Int
  const pending::Dict{Base.UUID,HTTP.Request}
  const processed::Dict{Base.UUID,HTTP.Request}
  const router::HTTP.Router
  server::Union{HTTP.Server,Nothing}
end

const RUNTIME_URL_BASE = "/2018-06-01/runtime"

function AWSRuntimeEmulator(host, port)
  router = HTTP.Router()
  aws = AWSRuntimeEmulator(host, port, Dict(), Dict(), router, nothing)

  # Register the LambdaResponder-facing API.
  HTTP.register!(router, "GET", RUNTIME_URL_BASE * "/invocation/next", Base.Fix1(send_next_invocation, aws))
  HTTP.register!(router, "POST", RUNTIME_URL_BASE * "/invocation/*/response", Base.Fix1(store_result, aws))
  HTTP.register!(router, "POST", RUNTIME_URL_BASE * "/invocation/*/error", Base.Fix1(throw_error, aws))
  aws
end

function start(aws::AWSRuntimeEmulator)
  server = HTTP.serve!(aws.router, aws.host, aws.port)
  @debug "Runtime emulator started at $(endpoint(aws.host, aws.port))"
  aws.server = server
end

function stop(aws::AWSRuntimeEmulator)
  isnothing(aws.server) && return
  HTTP.forceclose(aws.server)
  aws.server = nothing
end

Base.show(io::IO, aws::AWSRuntimeEmulator) =
  print(io, "AWSRuntimeEmulator(processed=", length(aws.processed), ", pending=", length(aws.pending), ')')

"""
Handle an incoming request, returning an HTTP error if an exception is thrown.

If request handling code is not wrapped in a similar way, whatever
sent the request will forever wait for a response.
"""
function handle_request(f)
  try
    f()
  catch exception
    e, bt = first(Base.catch_stack())
    server_error = """
    A failure occurred when processing request:

    $(sprint(showerror, e, bt; context = :color => true))
    """

    client_error = sprint(showerror, e)
    # Throw error to be caught by the backend logs.
    @error "$server_error"

    HTTP.Response(500)
  end
end

"""
Returns the appropiate status code based on the type of exception.
"""
get_exception_status_code(::InvalidJSONException) = 400
get_exception_status_code(e::Exception) = 500

"""
General API error response method.
"""
function error_response(message::String, e::Exception; response_code = 500)
  response = Dict(:error => true, :message => message)
  convert(HTTP.Response, response; response_code, headers = [BASE_HEADERS; "Content-Type" => "application/json"])
end

function send_next_invocation(aws::AWSRuntimeEmulator, req::HTTP.Request)
  handle_request() do
    while isempty(aws.pending)
      # Wait for requests to be submitted to the runtime.
      sleep(0.01)
    end
    id, pending = pop!(aws.pending)
    # Forward request to Lambda function with the required interface.
    body = Dict(
      :isBase64Encoded => true,
      :path => pending.target,
      :body => base64encode(isempty(pending.body) ? "{}" : String(pending.body)),
      :httpMethod => pending.method,
      # Add the server IP address to the mocked lambda interface object.
      :requestContext => Dict(:identity => Dict(:sourceIp => string(getipaddr()))),
    )
    HTTP.Response(200, ["Lambda-Runtime-Aws-Request-Id" => string(id)]; body = JSON3.write(body))
  end
end

function store_result(aws::AWSRuntimeEmulator, req::HTTP.Request)
  handle_request() do
    id = Base.UUID(only(match(r"/invocation/(.*)/response", req.target).captures))
    aws.processed[id] = req
    # Signal the Lambda that we successfully received the result.
    HTTP.Response(200)
  end
end

function throw_error(aws::AWSRuntimeEmulator, req::HTTP.Request)
  handle_request() do
    id = Base.UUID(only(match(r"/invocation/(.*)/error", req.target).captures))
    aws.processed[id] = req
    # Signal the Lambda that we successfully received the error.
    HTTP.Response(200)
  end
end

"Ask the runtime to call a Lambda function and return the result."
function call_lambda(aws::AWSRuntimeEmulator, req::HTTP.Request)
  handle_request() do
    id = Base.UUID(rand(UInt128))
    aws.pending[id] = req
    @debug "Request with id $id pending for execution."
    t = 0
    while !haskey(aws.processed, id) && t < 60
      # Wait for Lambda function to produce results.
      t += @elapsed sleep(0.01)
      t > 59.9 && return HTTP.Response(408; body = "Timeout while waiting for lambda function to execute.")
    end
    processed = pop!(aws.processed, id)
    @debug "Received processed request."
    body = JSON3.read(processed.body)
    if haskey(body, :errorType)
      HTTP.Response(
        500;
        body = string(
          "Error of type ",
          body.errorType,
          " returned from the Lambda with the following message: ",
          body.errorMessage,
        ),
      )
    else
      HTTP.Response(body[:statusCode]; body = body[:body])
    end
  end
end
