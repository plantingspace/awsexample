import AWSExample: start, stop

"""
An emulation of an API Gateway specifically configured for our backend. It uses a lambda function (emulated via the [`AWSRuntimeEmulator`](@ref)) for the backend.

Note that it is not meant to really emulate the general functionality provided by the AWS API Gateway, but merely exposes endpoints which abstract over the way the lambda must be called.
"""
mutable struct APIGateway
  host::String
  port::Int
  const router::HTTP.Router
  server::Union{HTTP.Server,Nothing}
end

function APIGateway(server::AWSRuntimeEmulator; host = "127.0.0.1", port = 8082)
  router = HTTP.Router()
  HTTP.register!(router, "/*", req -> call_lambda(server, req))
  APIGateway(host, port, router, nothing)
end

function start(gateway::APIGateway)
  server = HTTP.serve!(gateway.router, gateway.host, gateway.port)
  @debug "Gateway started at $(string(gateway.host, ':', gateway.port))"
  gateway.server = server
end

function stop(gateway::APIGateway)
  isnothing(gateway.server) && return
  HTTP.forceclose(gateway.server)
  gateway.server = nothing
end

endpoint(gateway::APIGateway, args...; kwargs...) = endpoint(gateway.host, gateway.port, args...; kwargs...)
