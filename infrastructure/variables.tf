# Input variables. They will be set automatically if matching `TF_VAR_<name>` environment variables are detected.

variable "aws_region" {
  type = string
}

variable "aws_ecr" {
  type = string
}

variable "image_tag" {
  type = string
}
