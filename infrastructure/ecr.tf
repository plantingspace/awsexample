resource "aws_ecr_repository" "awsexample_repo" {
  name = "awsexample"
  image_scanning_configuration {
    scan_on_push = true
  }
}
