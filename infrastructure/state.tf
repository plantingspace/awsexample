# These resources are meant to setup an S3 backend in which to store the state of the infrastructure. A lock is used for proper synchronization to avoid potential concurrency issues.
# Remember that Terraform is stateless, and relies on this "manifest" state file to know about the state of deployed resources.

resource "aws_s3_bucket" "tfstate" {
  bucket = "juliacon-tfstate"
  versioning {
    enabled = true
  }
}

resource "aws_s3_bucket_public_access_block" "tfstate" {
  bucket = aws_s3_bucket.tfstate.id

  block_public_acls       = true
  block_public_policy     = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_ownership_controls" "tfstate" {
  bucket = aws_s3_bucket.tfstate.id

  rule {
    object_ownership = "BucketOwnerEnforced"
  }
}

resource "aws_dynamodb_table" "tfstate_lock" {
  hash_key = "LockID"
  name     = "tfstate-lock"
  attribute {
    name = "LockID"
    type = "S"
  }
  billing_mode = "PAY_PER_REQUEST"

  tags = {
    CostAllocation = "Infrastructure"
  }
}
