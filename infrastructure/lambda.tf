resource "aws_iam_role" "lambda_iam_role" {
  name = "lambda_iam_role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Sid    = ""
      Principal = {
        Service = "lambda.amazonaws.com"
      }
    }]
  })
}

resource "aws_lambda_function" "awsexample_lambda" {
  function_name = "awsexample"
  package_type  = "Image"
  image_uri     = "${var.aws_ecr}/${aws_ecr_repository.awsexample_repo.name}:${var.image_tag}"
  publish       = true
  memory_size   = 1024
  timeout       = 30
  role          = aws_iam_role.lambda_iam_role.arn
}

resource "aws_lambda_function_url" "awsexample_lambda_url" {
  function_name      = aws_lambda_function.awsexample_lambda.function_name
  authorization_type = "NONE"

  cors {
    allow_credentials = true
    allow_origins     = ["*"]
    allow_methods     = ["*"]
    allow_headers     = ["date", "keep-alive"]
    expose_headers    = ["keep-alive", "date"]
    max_age           = 86400
  }
}

resource "aws_cloudwatch_log_group" "awsexample_lambda_logs" {
  name              = "/aws/lambda/awsexample"
  retention_in_days = 30
}

resource "aws_iam_role_policy_attachment" "lambda_iam_policy_attachment" {
  role       = aws_iam_role.lambda_iam_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}
