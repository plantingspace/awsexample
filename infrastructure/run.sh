#!/bin/bash

# Use this to run Terraform locally.
# WARNING: this requires a functional backend (e.g. S3). Run this only if you know what you are doing.

export IMAGE_TAG="v20230720-74f4381a" # replace this with the desired image tag (e.g. the latest one)
export AWS_ACCOUNT_ID=094015711163 # replace with your account ID
export AWS_REGION="eu-central-1" # replace with the region of your choice
export AWS_ECR="$AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com"
export TF_VAR_aws_ecr=$AWS_ECR
export TF_VAR_aws_region=$AWS_REGION
export TF_VAR_image_tag=$IMAGE_TAG

terraform init
terraform apply -auto-approve -lock-timeout=5m
# terraform destroy
