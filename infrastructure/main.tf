terraform {
  backend "s3" {
    bucket         = "juliacon-tfstate"
    key            = "juliacon/awsexample.tfstate"
    region         = "eu-central-1"
    dynamodb_table = "tfstate-lock"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.5"
    }
  }

  required_version = ">= 1.2.0"
}

provider "aws" {
  region = var.aws_region
}
