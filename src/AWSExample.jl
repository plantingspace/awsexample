module AWSExample

using JSON3
using HTTP
using Pkg: project
using Pkg.Types: EnvCache
using Dates: now
using InteractiveUtils: peakflops
using Base64: base64decode

export LambdaResponder, start, stop, deploy

const BASE_HEADERS = [
  # CORS headers.
  "Access-Control-Allow-Origin" => "*",
  "Access-Control-Allow-Headers" => "*",
  "Access-Control-Allow-Methods" => "POST, GET, OPTIONS",
]

"""
Handler for Lambda functions, which communicates with the AWS Runtime during execution.
"""
mutable struct LambdaResponder{T}
  """
  Address of the runtime with which to communicate. By default, this will be filled with the value
  of `AWS_LAMBDA_RUNTIME_API`, which is automatically setup by AWS. This can be set to another value for testing purposes.
  """
  const api_address::Union{Nothing,String}
  const data::T
  "Task for the interfacing with the AWS runtime, stored to allow for interruptions."
  task::Union{Task,Nothing}
  LambdaResponder(data = nothing; api_address = get(ENV, "AWS_LAMBDA_RUNTIME_API", nothing)) =
    new{typeof(data)}(api_address, data, nothing)
end

function runtime_url(resp::LambdaResponder)
  !isnothing(resp.api_address) || error("No API address defined for $resp")
  "http://$(resp.api_address)/2018-06-01/runtime"
end

"Call the function designated by `f` with `args...`, as requested by the runtime."
function dispatch(resp::LambdaResponder, f::Symbol, args...)
  if f === :test
    (; version) = project(EnvCache(joinpath(pkgdir(@__MODULE__), "Project.toml")))
    Dict(:package_version => version, :date => now())
  elseif f === :peakflops
    worksize = only(args)
    peakflops(worksize)
  elseif f === :eval
    # WARNING: do not use that in an actual production environment, for obvious security reasons.
    code = only(args)
    ex = Meta.parse(code)
    eval(ex)
  else
    error("Function '$f' is not supported")
  end
end

"Parse arguments in JSON form"
function parse_args(f::Symbol, request_body::Union{Nothing,AbstractString})
  f === :test && return ()
  parameters = JSON3.read(request_body)
  if f === :peakflops
    worksize = get(parameters, :worksize, nothing)
    isnothing(worksize) && throw(InvalidJSONException("A `worksize` field is required."))
    isa(worksize, Number) || throw(InvalidJSONException("A number expected as `worksize`, got $(typeof(worksize))."))
    worksize
  elseif f === :eval
    code = get(parameters, :code, nothing)
    isnothing(code) && throw(InvalidJSONException("A `code` field is required."))
    code
  else
    error("Unexpected function '$f'")
  end
end

struct InvalidJSONException <: Exception
  message::String
end

Base.showerror(io::IO, e::InvalidJSONException) = print(io, e.message)

"""
    start(resp::LambdaResponder)

Start the `LambdaResponder` which will continuously process requests
obtained from the runtime API invocation address.
"""
function start(resp::LambdaResponder)
  isnothing(resp.task) || error("The LambdaResponder has already been started.")
  resp.task = @async serve_requests(resp)
end

function stop(resp::LambdaResponder)
  isnothing(resp.task) && return
  stop(resp.task)
  resp.task = nothing
end
function stop(task::Task)
  try
    t = schedule(task, InterruptException(), error = true)
    wait(t)
  catch
  end
end

deploy() = deploy(LambdaResponder())
deploy(resp::LambdaResponder) = serve_requests(resp)

macro lambda_debug(ex)
  prefix = string(Base.text_colors[:yellow], "Lambda function: ", Base.text_colors[:default])
  Expr(
    :macrocall,
    Symbol("@debug"),
    __source__,
    :(string($prefix, $(esc(ex)))),
  )
end

"""
Return the answer, conform to the API format.
https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-output-format
"""
function send_answer(url, runtime_request_id, answer, status_code = 200)
  headers = isnothing(answer) ? BASE_HEADERS : [BASE_HEADERS; "Content-Type" => "application/json"]
  send_response(build_response(status_code, answer, headers), url, runtime_request_id)
end

function send_error(url, runtime_request_id, message::String, status_code = 500)
  headers = [BASE_HEADERS; "Content-Type" => "application/json"]
  answer = Dict(:error => true, :message => message)
  send_response(build_response(status_code, answer, headers), url, runtime_request_id)
end

function build_response(status_code, answer, headers)
  Dict(
    :isBase64Encoded => false,
    :statusCode => status_code,
    :body => isnothing(answer) ? "" : JSON3.write(answer),
    :headers => Dict(headers),
  )
end

function send_response(response, url, runtime_request_id)
  @lambda_debug "Returning answer $(sprint(show, MIME"text/plain"(), response))"
  response = HTTP.post(
    "$url/invocation/$runtime_request_id/response",
    [BASE_HEADERS; "Content-Type" => "application/json"],
    JSON3.write(response),
  )
  @lambda_debug "Lambda runtime notified"
  !HTTP.iserror(response) ||
    send_runtime_error(url, runtime_request_id, ErrorException("Could not notify the Lambda runtime: $response"))
end

# Additional fields may be used with a custom logger to include further information for debugging purposes.
const additional_log_fields = Dict{Symbol,Any}()

function next_request(endpoint)
  response = HTTP.get(endpoint; status_exception = false)
  if HTTP.iserror(response)
    @error "Could not get next request: $response"
    nothing
  else
    response
  end
end

function extract_request(response::HTTP.Response)
  runtime_request_id_idx = findfirst(==("Lambda-Runtime-Aws-Request-Id") ∘ first, response.headers)
  if isnothing(runtime_request_id_idx)
    @error "Received a request from Lambda AWS with no request id"
    nothing
  else
    last(response.headers[runtime_request_id_idx])
  end
end

function serve_requests(resp::LambdaResponder)
  url = runtime_url(resp)
  next_endpoint = "$url/invocation/next"
  while true
    # Get the next request.
    @lambda_debug "Waiting for next request at $next_endpoint..."

    response = @something(next_request(next_endpoint), continue)
    runtime_request_id = @something(extract_request(response), continue)

    # Do some early logging in case JSON parsing of the request body throws an exception.
    @lambda_debug "Received incoming request with runtime request id $runtime_request_id"
    request_body = JSON3.read(response.body)
    !haskey(request_body, :requestContext) && error(
      "It seems that the request did not go through an API Gateway. If so, please provide a body containing values for `:requestContext`, `:path` and `:httpMethod`.",
    )
    gateway_request_id = get(request_body[:requestContext], :requestId, "")

    # Dispatch the contents of the request to the right function.
    parts = split(request_body[:path], '/')
    f = Symbol(last(parts))

    # Store additional log fields early so they are tied to each log entry for this request.
    additional_log_fields[:runtime_request_id] = runtime_request_id
    additional_log_fields[:gateway_request_id] = gateway_request_id
    additional_log_fields[:endpoint] = string(f)

    @lambda_debug "Processing request with gateway request id $gateway_request_id"

    # Print the raw payload body in the logs(makes it easy to reproduce the request when tesing).
    @lambda_debug """
    Received request body: $(sprint(show, MIME"text/plain"(), String(response.body)))
    """

    # Handle CORS preflight request.
    if request_body[:httpMethod] == "OPTIONS"
      # TODO: Perform validation on the origin (`body[:origin]`).
      send_answer(url, runtime_request_id)
      continue
    end

    answer = try
      body = request_body[:body]
      get(request_body, :isBase64Encoded, false) && (body = String(base64decode(body)))
      args = parse_args(f, body)
      # Replace args with request_data serialised.
      @lambda_debug "Running '$f' with arguments $args"
      dispatch(resp, f, args)
    catch e
      message, bt = first(Base.catch_stack())

      # Manually call `showerror` to still display the stacktrace with Logging.SimpleLogger().
      @error """An error occurred while processing the request (runtime request id: $runtime_request_id, gateway request id: $gateway_request_id function: $f)

      $(sprint(showerror, message, bt))
      """

      client_error = sprint(showerror, message)

      # User facing errors are returned as a normal lambda response.
      if isa(e, InvalidJSONException)
        send_error(url, runtime_request_id, client_error, get_exception_status_code(e))
      else
        send_runtime_error(url, runtime_request_id, e)
      end

      isa(e, InterruptException) && break
      continue
    end

    try
      send_answer(url, runtime_request_id, answer)
    catch e
      handle_exception(e, url, runtime_request_id, gateway_request_id)
      isa(e, InterruptException) && break
    finally
      # Forget logging context.
      empty!(additional_log_fields)
    end
  end
end

function handle_exception(e, url, runtime_request_id, gateway_request_id)
  @error """An error occurred while returning the result for runtime request id $runtime_request_id, gateway request id $gateway_request_id
  $(sprint(showerror, first(Base.catch_stack())...))
  """
  send_runtime_error(url, runtime_request_id, e)
end

"Report an error to the AWS Lambda runtime API."
function send_runtime_error(url, runtime_request_id, exception::Exception)
  url = "$url/invocation/$runtime_request_id/error"
  headers = ["Content-Type" => "application/json", "Lambda-Runtime-Function-Error-Type" => "Unhandled"]
  errmsg = sprint(showerror, exception)
  try
    response = HTTP.post(
      url,
      [BASE_HEADERS; headers],
      JSON3.write(Dict(:errorType => string(typeof(exception)), :errorMessage => errmsg)),
    )
    !HTTP.iserror(response) || error("Could not notify the Lambda runtime of an error: $response")
  catch failure
    @error """
      A secondary error occurred while trying to notify the lambda runtime of an error:
        url: $url
        original error: $errmsg
        notification error: $(sprint(showerror, failure))
    """
  end
end

end
