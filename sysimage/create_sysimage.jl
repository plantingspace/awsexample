using InteractiveUtils: versioninfo
using ArgMacros

import PackageCompiler

function create_sysimage(
  sysimage_output::String;
  base_sysimage::String = "",
  cpu_target::String = "",
  precompile_statements_file = "",
  precompile_execution_file = "",
)
  root = dirname(@__DIR__)
  parameters = Dict{Symbol,Any}(
    :project => root,
    :sysimage_path => joinpath(root, sysimage_output),
    :precompile_execution_file => precompile_execution_file,
  )

  if isempty(base_sysimage)
    # Filter stdlibs to trim down the size of the image.
    # To filter out unnecessary stdlibs, `incremental` must be `false`.

    # XXX: This does not work robustly yet; our `peakflops` endpoint does not work properly with these.
    # parameters[:filter_stdlibs] = true
    # parameters[:incremental] = false
  else
    # If a base image is chosen, `incremental` must be `true` (see PackageCompiler docs).
    parameters[:base_sysimage] = base_sysimage
    parameters[:incremental] = true
  end

  # Future improvement: set target based on argument string, specific for different CI architectures (arm, avx512 intel, ...)
  # Note: for incremental builds, both the base image and the new image must be build with the same targets.
  if !isempty(cpu_target)
    parameters[:cpu_target] = cpu_target
  end

  if !isempty(precompile_statements_file)
    parameters[:precompile_statements_file] = joinpath(root, precompile_statements_file)
  end

  image_kwargs = NamedTuple(parameters)
  println("Build sysimage with kwargs: $image_kwargs")
  PackageCompiler.create_sysimage(:AWSExample; image_kwargs...)
end

@eval function main()
  @inlinearguments begin
    @helpusage "julia sysimage/create_sysimage.jl [-p]"
    @helpdescription """
    Create a system image with PackageCompiler, to speed up starting time for either Continuous Integration (CI) or a production environment.

    For CI, all package dependencies of AWSExample will be saved, but no code related to AWSExample directly will be loaded or compiled,
    such that changes to AWSExample will always be taken into account.
    For production, AWSExample is loaded and included in the sysimage. This greatly reduces
    latency for the first API calls, but any changes to AWSExample will be ignored.
    """
    @argumentrequired String sysimage_output "-o" "--output"
    @arghelp "Output location for the created sysimage. Required argument. Path should be relative to the AWSExample folder."
    @argumentdefault String "" base_sysimage "-b" "--base_sysimage"
    @arghelp "Specify a base system image to be incrementally updated to create the new one."
    @argumentdefault String "" cpu_target "-c" "--cpu_target"
    @arghelp "A list of LLVM features to be targeted (see https://docs.julialang.org/en/v1/devdocs/sysimg/#Specifying-multiple-system-image-targets)"
    @argumentdefault String "" precompile_statements_file "-f" "--precompile_statements_file"
    @arghelp "A file of precompile statements that serve as directives to precompile additional methods into the sysimage. Path should be relative to the AWSExample folder. "
    @argumentdefault String "" precompile_execution_file "-f" "--precompile_execution_file"
    @arghelp "A file to be executed with the aim of recording precompile statements. Path should be relative to the AWSExample folder. "
  end

  @info "Creating system image"
  println()
  versioninfo()
  @time create_sysimage(
    sysimage_output;
    base_sysimage,
    cpu_target,
    precompile_statements_file,
    precompile_execution_file,
  )
end

main()
