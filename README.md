# AWSExample

**[JuliaCon 2023 Talk](https://pretalx.com/juliacon2023/talk/3QFPST/)** — **[Slides](.assets/slides.pdf)**

Example project to show how to perform a basic deployment of a Julia application with a serverless architecture. This project is meant as an inspiration and a base onto which to build your own application using your own tech stack. You may also find [JuliaCloud/LambdaMaker.jl](https://github.com/JuliaCloud/LambdaMaker.jl/) of interest.

Tools and services from AWS will be used throughout this project, but the general workflow should remain similar for non-AWS services which support deployment via Docker containers.

We encourage you to take a look around, and consult the [latest pipelines](https://gitlab.com/plantingspace/awsexample/-/pipelines) as well as:
- The [GitLab configuration file](https://gitlab.com/plantingspace/awsexample/-/blob/main/.gitlab-ci.yml), in particular its `deploy application` job which builds the Docker image, pushes it to the AWS ECR and configures the Lambda through Terraform;
- The [Dockerfile](https://gitlab.com/plantingspace/awsexample/-/blob/main/Dockerfile), to see how the Docker image gets built;
- The [infrastructure folder](https://gitlab.com/plantingspace/awsexample/-/tree/main/infrastructure), to take a look at the various Terraform-managed resources and configurations involved in deploying a serverless application;
- [Source files](https://gitlab.com/plantingspace/awsexample/-/tree/main/src), which implement the integration with the AWS Runtime;
- [Test files](https://gitlab.com/plantingspace/awsexample/-/tree/main/test), which contain a convenient test harness with an AWS Runtime emulator.

## Scope

This project is aimed at showing an example of a fully functional Julia application deployed via [AWS Lambda](https://aws.amazon.com/lambda/). This is not a tutorial, nor a reference material. This is not a library either: it would be challenging to design one that satisfies the setups and tech stacks of everyone. Instead of trying to abstract over it, we prefer to show what has worked for us, so you can translate that into a setup that works for you. You can try the [JuliaCloud/LambdaMaker.jl](https://github.com/JuliaCloud/LambdaMaker.jl/) project generator which relies on the [AWS Serverless Application Model (SAM)](https://aws.amazon.com/serverless/sam/) for deployment.

Basic familiarity with Julia and DevOps is assumed. There is already plenty of material online to learn both, and we prefer to keep the explanations on point. We will nonetheless try to abstract over technical jargon whenever possible.

## Status

Although this example project has been successfully deployed, we decided to decommision the Lambda function due to:
- the obligation to use a private AWS ECR repository to host the Docker image (public repositories are not supported),
- the inability to limit the infrastructure costs, should demand suddenly increase for whatever reason (only notification alerts may be configured).

All the code remains unchanged, and you should be able to replicate a working application deployment based on this example project.

## General steps

The steps to deploying a Julia application via a serverless architecture may be presented roughly as follows:
- Write a Julia package with a component that interacts with the infrastructure used for deployment (here, the AWS Lambda). This component is generally called a [runtime](https://docs.aws.amazon.com/lambda/latest/dg/lambda-runtimes.html) and functions as a relay between the Lambda infrastructure and the desired code. Unfortunately, there is no official or unofficial runtime yet. 
- (optional but recommended) Test your interface via a set of tests, emulating external runtime components as necessary.
- Produce a Dockerfile to build a Docker container with your Julia application, including any solutions to reduce latency (e.g. using system images with PackageCompiler.jl).
- Setup the infrastructure required to support the deployment, e.g. automated using Terraform.
- Write a CI job to automate the deployment of your application, from building the Docker image to deploying it on your serverless solution.

## Building the Docker container

There is nothing special about building a Docker to ship Julia code. The main bits that are specific to the language are its installation, and precompiling as much as possible to reduce "time-to-first-answer" latency.

For the installation, we use the [official Julia Docker image](https://hub.docker.com/_/julia) as a base image, and then install a number of tools such as the AWS CLI and a runtime interface emulator (to test the final image locally).

To reduce latency, we use [PackageCompiler.jl](https://github.com/JuliaLang/PackageCompiler.jl) and use the test suite `test/runtests.jl` to generate precompilation statements. Note that in this approach it is crucial to have a properly emulated AWS runtime so that the code related to the inferface with the AWS runtime can be fully compiled.

Once an image is deployed, you should be able to test it with the [AWS Lambda Runtime Emulator](https://github.com/aws/aws-lambda-runtime-interface-emulator) (which is bundled in the Docker image, local installation is therefore not required) using
```bash
docker run -it -p 9000:8080 <aws_ecr>/<image>:<tag>
```
and calling the endpoints on another process
```bash
curl -X GET http://localhost:9000/2015-03-31/functions/function/invocations -d '{"path": "/test", "body": "", "httpMethod": "", "requestContext": {}}'
curl -X GET http://localhost:9000/2015-03-31/functions/function/invocations -d '{"path": "/peakflops", "body": "{\"worksize\": 2000}", "httpMethod": "", "requestContext": {}}'
curl -X GET http://localhost:9000/2015-03-31/functions/function/invocations -d '{"path": "/eval", "body": "{\"code\": \"1 + 1\"}", "httpMethod": "", "requestContext": {}}'
```

Note that the `path`, `body`, `httpMethod` and `requestContext` fields are normally generated by the API Gateway. In this case, we don't go through it, so we must forward additional information.

## Infrastructure

The deployed service makes use of a number of components:
- A Docker container registry managed by AWS, the [**AWS ECR** (Elastic Container Registry)](https://aws.amazon.com/ecr/). This is where we'll store our Docker images, to be used by AWS to spin up workers for the lambda.
- An [**AWS Lambda**](https://aws.amazon.com/lambda/) function, which spawns workers and dispatches requests to available workers to execute the application logic.
- An [**API Gateway**](https://aws.amazon.com/api-gateway/), which defines a web API for your backend, to be queried e.g. from a webservice frontend. This is where routing is managed, and requests are automatically dispatched to lambdas (e.g. you could have different lambdas for different endpoints).
- [**Cloudwatch**](https://aws.amazon.com/cloudwatch/), a logging system for the Lambda to introspect into invocations. This is mostly used for troubleshooting and comes by default with AWS Lambda, but requires setting up a few permissions.

In a more production oriented environment, you would typically export labelled metrics (e.g. to [Prometheus](https://prometheus.io/)), in addition to Cloudwatch, and have the ability to visualize both (e.g. via [Grafana](https://grafana.com/)).

A typical webservice would additionally have frontend component(s), which are not part of this example project; only the backend has been implemented.

These components are managed by [Terraform](https://www.terraform.io/), an industry standard for the management and configuration of infrastructure resources. Terraform files are available under `/infrastructure`.

### Creating the AWS ECR repository

Lambda functions may be deployed from either zip archives (for certain supported languages) or Docker images. For a Julia based project, we need to use Docker images, and we need a place in which to store them. Unfortunately, at the moment it seems that AWS Lambda only supports images from private AWS ECR repositories; we hoped to use a public repository for this example to also make the Docker images available for download.

The creation of the repository is managed via Terraform, with the resource declaration `resource "aws_ecr_repository" "awsexample_repo" { ... }`. Alternatively, you can create one in the AWS ECR console manually via the web interface.

### Pushing to the AWS ECR

After building a Docker image, you can push it to the AWS ECR using [a credentials helper](https://github.com/awslabs/amazon-ecr-credential-helper), with [an appropriate configuration](https://github.com/awslabs/amazon-ecr-credential-helper#configuration).

Before attempting to authenticate and push to the ECR from CI, we recommend to make sure it all works locally. Docker has a very bad way of handling errors for pushes, and may not tell you why the push failed; if the repo doesn't exist, it won't tell you, and if the credentials are not correct, it won't tell you either.

## Known limitations

- The first call to a lambda on cold starts seemed to take a lot of time, enough to time out after 30s, despite using a system image. We do not know the cause and could not reproduce locally: using the AWS runtime emulator with the same Docker image showed very small startup times. This effect may be mitigated by keeping the Lambda warm (e.g. pinged every minute or so), but could warrant a more detailed investigation.
